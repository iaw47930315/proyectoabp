<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    public function scopeSearch($query, $accessories) {
        return $query->where('name', 'LIKE', '%' . $accessories . '%');
    }

    public function scopeCategory($query, $accessories) {
        return $query->where('category', 'LIKE', '%' . $accessories . '%');
    }

    public function scopeOrder($query, $search, $order) {
        return $query->orderBy($search, $order);
    }

    public function scopeShow($query, $limit) {
        return $query->limit($limit);
    }

    public function scopePriceMin($query,$input){
        return $query->where('price','>',$input);
    }

    public function scopePriceMax($query,$input){
        return $query->where('price','<',$input);
    }

    public function scopeAvailable($query,$input){
        return $query->where('available',$input);
    }
}
