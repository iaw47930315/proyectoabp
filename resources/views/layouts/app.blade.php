<!DOCTYPE html>
<html lang="en">

<!-- Head BEGIN -->
<head>
    <meta charset="utf-8">
    <title>Categoria</title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta content="Metronic Shop UI description" name="description">
    <meta content="Metronic Shop UI keywords" name="keywords">
    <meta content="keenthemes" name="author">

    <meta property="og:site_name" content="-CUSTOMER VALUE-">
    <meta property="og:title" content="-CUSTOMER VALUE-">
    <meta property="og:description" content="-CUSTOMER VALUE-">
    <meta property="og:type" content="website">
    <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
    <meta property="og:url" content="-CUSTOMER VALUE-">

    <script src="{{asset('plugins/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('corporate/scripts/back-to-top.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script src="{{asset('plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
    <script src="{{asset('plugins/owl.carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->
    <script src="{{asset('plugins/zoom/jquery.zoom.min.js')}}" type="text/javascript"></script><!-- product zoom -->
    <script src="{{asset('plugins/bootstrap-touchspin/bootstrap.touchspin.js')}}" type="text/javascript"></script><!-- Quantity -->
    <script src="{{asset('plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/rateit/src/jquery.rateit.js')}}" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script><!-- for slider-range -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="{{asset('corporate/scripts/layout.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            Layout.init();
            Layout.initOWL();
            Layout.initTwitter();
            Layout.initImageZoom();
            Layout.initTouchspin();
            Layout.initUniform();
            //Layout.initSliderRange();
        });
    </script>

    <link rel="shortcut icon" href="favicon.ico">

    <!-- Fonts START -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
    <!-- Fonts END -->

    <!-- Global styles START -->
    <link href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Global styles END -->

    <!-- Page level plugin styles START -->
    <link href="{{asset('plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/owl.carousel/assets/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css">
    <link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css"><!-- for slider-range -->
    <link href="{{asset('plugins/rateit/src/rateit.css')}}" rel="stylesheet" type="text/css">
    <!-- Page level plugin styles END -->

    <!-- Theme styles START -->
    <link href="{{asset('pages/css/components.css')}}" rel="stylesheet">
    <link href="{{asset('corporate/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('pages/css/style-shop.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('corporate/css/style-responsive.css')}}" rel="stylesheet">
    <link href="{{asset('corporate/css/themes/red.css')}}" rel="stylesheet" id="style-color">
    <link href="{{asset('corporate/css/custom.css')}}" rel="stylesheet">
    <!-- Theme styles END -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" rel="stylesheet" type="text/css">
</head>
<!-- Head END -->

<!-- Body BEGIN -->

<body class="ecommerce">
  <!-- BEGIN HEADER -->
  <div class="header">
    <div class="container">
      <!-- BEGIN TOP SEARCH -->
      <div class="search-box align-self-xl-center">
        <a class="site-logo" href="category">
          <img src="{{asset('corporate/img/logos/logo.png')}}" alt="AFK" style="width: 100%">
        </a>
        <form class="form-search content-search navbar-form" action="/categorySearch" method="post">
            @csrf
          <div class="input-group">
            <span class="input-group-btn">
              <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bars"></i>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="/category/anells">Anells</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/barrets">Barrets</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/bolsos">Bolsos</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/bufandes">Bufandes</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/collars">Collars</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/guants">Guants</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/mascaretes">Mascaretes</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/pendentss">Pendents</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/polseres">Polseres</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/rellotges">Rellotges</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/ulleres">Ulleres</a></li>
                </ul>
              </div>
            </span>
            <input placeholder="Buscar" class="form-control form-text" type="text" size="75" maxlength="128" name="search">
            <span class="input-group-btn">
                <button class="btn btn-primary" type="submit"><span class="icon glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </span>
          </div>
          @if (app('request')->session()->get('Usuario'))
            <a href="/profile" class="btn btn-info btn-lg" style="color:#000; background-color:#fff; border-color:#fff;">
          @else
            <a href="/login" class="btn btn-info btn-lg" style="color:#000; background-color:#fff; border-color:#fff;">
          @endif
            <span class="glyphicon glyphicon-user"></span>
            {{ app('request')->session()->get('Usuario')['name'] ?? 'login'}}
          </a>
        </form>
        <!-- BEGIN CART -->

        <div class="top-cart-block">
          <div class="top-cart-content-wrapper">
            <div class="top-cart-content">
              <ul class="scroller" style="height: 250px;">
              @php
                $totalPrice = 0;
                $carrito = app('request')->session()->get('Usuario')['carrito'] ?? [];
                $id = 0;
              @endphp
              @forelse ($carrito as $producto)
                <li>
                  <a href="shop-item.html"><img src="{{asset('pages/img/products/').'/'. $producto['img'] }}" alt="$producto['name']" width="37" height="34"></a>
                  <span class="cart-content-count">x 1</span>
                  <strong><a href="shop-item.html">{{ $producto['name'] }}</a></strong>
                  <em>{{ $producto['price'] }}€</em>
                  <a href="/removeProductFromCart/{{$id}}" class="del-goods">&nbsp;</a>
                </li>
                @php
                  $totalPrice += $producto['price'];
                  $id++;
                @endphp
              @empty
                <p>No hi ha productes a la cistella</p>
              @endforelse
              </ul>
              <div class="text-right">
                <a href="/removeAllCart" class="btn btn-default">Buidar cistella</a>
                <a href="/cart" class="btn btn-default">Veure cistella</a>
                <a href="confirmacion.html" class="btn btn-primary">Confirmar</a>
              </div>
            </div>
          </div>
          <div class="top-cart-info">
            <a class="top-cart-info-count">{{sizeof($carrito)}} productes</a>
            <a class="top-cart-info-value">{{$totalPrice}}€</a>
          </div>
          <i class="fa fa-shopping-cart"></i>
        </div>
        <!--END CART -->
      </div>
      <!-- END TOP SEARCH -->
    </div>
  </div>
  <!-- Header END -->

  @yield('content')

  <!-- BEGIN PRE-FOOTER -->
  <div class="pre-footer">
    <div class="container">
      <div class="row">
        <!-- BEGIN BOTTOM ABOUT BLOCK -->
        <div class="col-md-4 col-sm-6 pre-footer-col">
          <h2>Sobre nosaltres</h2>
          <p>Descripció</p>
        </div>
        <!-- END BOTTOM ABOUT BLOCK -->
        <!-- BEGIN BOTTOM INFO BLOCK -->
        <div class="col-md-4 col-sm-6 pre-footer-col">
          <h2>Informació</h2>
          <ul class="list-unstyled">
            <li><i class="fa fa-angle-right"></i> <a href="">Contacte</a></li>
          </ul>
        </div>
        <!-- END INFO BLOCK -->

        <!-- BEGIN BOTTOM CONTACTS -->
        <div class="col-md-4 col-sm-6 pre-footer-col">
          <h2>Formes de contacte</h2>
          <address class="margin-bottom-40">
            Carrer del Comte d'Urgell, 187<br>
            Barcelona<br>
            Telefón: 93 430 92 00<br>
            Emails:   <a href="mailto:correoDeContacto1@gmail.com">correoDeContacto1@gmail.com</a><br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="mailto:correoDeContacto2@gmail.com">correoDeContacto2@gmail.com</a><br>
          </address>
        </div>
        <!-- END BOTTOM CONTACTS -->
      </div>
    </div>
  </div>
  <!-- END PRE-FOOTER -->

  <!-- BEGIN FOOTER -->
  <div class="footer">
    <div class="container">
      <div class="row">
        <!-- BEGIN COPYRIGHT -->
        <div class="col-md-6 csm-4 padding-top-10">
          2020 © AFK. ALL Rights Rol-eserved.
        </div>
        <!-- END COPYRIGHT -->
      </div>
    </div>
  </div>
  <!-- END FOOTER -->

  <!-- BEGIN fast view of a product -->
  <div id="product-pop-up" style="display: none; width: 700px;">
    <div class="product-page product-pop-up">
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-3">
          <div class="product-main-image">
            <img src="{{asset('pages/img/products/model7.jpg')}}" alt="Cool green dress with red bell" class="img-responsive">
          </div>
          <div class="product-other-images">
            <a href="javascript:;" class="active"><img alt="Berry Lace Dress" src="{{asset('pages/img/products/model3.jpg')}}"></a>
            <a href="javascript:;"><img alt="Berry Lace Dress" src="{{asset('pages/img/products/model4.jpg')}}"></a>
            <a href="javascript:;"><img alt="Berry Lace Dress" src="{{asset('pages/img/products/model5.jpg')}}"></a>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-9">
          <h2>Cool green dress with red bell</h2>
          <div class="price-availability-block clearfix">
            <div class="price">
              <strong><span>$</span>47.00</strong>
              <em>$<span>62.00</span></em>
            </div>
            <div class="availability">
              Availability: <strong>In Stock</strong>
            </div>
          </div>
          <div class="description">
            <p>Lorem ipsum dolor ut sit ame dolore  adipiscing elit, sed nonumy nibh sed euismod laoreet dolore magna aliquarm erat volutpat
              Nostrud duis molestie at dolore.</p>
            </div>
            <div class="product-page-options">
              <div class="pull-left">
                <label class="control-label">Size:</label>
                <select class="form-control input-sm">
                  <option>L</option>
                  <option>M</option>
                  <option>XL</option>
                </select>
              </div>
              <div class="pull-left">
                <label class="control-label">Color:</label>
                <select class="form-control input-sm">
                  <option>Red</option>
                  <option>Blue</option>
                  <option>Black</option>
                </select>
              </div>
            </div>
            <div class="product-page-cart">
              <div class="product-quantity">
                <input id="product-quantity" type="text" value="1" readonly name="product-quantity" class="form-control input-sm">
              </div>
              <button class="btn btn-primary" type="submit">Add to cart</button>
              <a href="shop-item.html" class="btn btn-default">More details</a>
            </div>
          </div>

          <div class="sticker sticker-sale"></div>
        </div>
      </div>
    </div>
    <!-- END fast view of a product -->

    <!-- Load javascripts at bottom, this will reduce page load time -->
    <!-- BEGIN CORE PLUGINS(REQUIRED FOR ALL PAGES) -->
    <!--[if lt IE 9]>
	<script src="{{asset('plugins/respond.min.js')}}"></script>
	<![endif]-->

    <!-- END PAGE LEVEL JAVASCRIPTS -->
  </body>
  <!-- END BODY -->
  </html>
