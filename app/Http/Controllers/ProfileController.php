<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\ProfileRequest;
use Illuminate\Support\Facades\DB;
use App\Services\UploadProfilePicService;

use App\Exceptions\UploadFileException;

class ProfileController extends Controller
{
    public $user;
    private $uploadService;

    public function __construct() {
        $this->user = new User;
    }
    // Perfil
    public function showProfile() {
        return view('profile');
    }

    public function closeSession(Request $request) {
        $request->session()->forget('Usuario');
        $accessories = Item::all();
        return redirect('/category')->with("accessories", $accessories);
    }

    // Configuracion
    public function setting() {
        return view('setting');
    }

    public function updateSetting(ProfileRequest $request, UploadProfilePicService $UploadProfilePicService) {
        $actualUser = app('request')->session()->get('Usuario');
        $email = $actualUser['email'];
        $updateUser = User::where('email', '=', $email)->first();

        if ($request->filled('name')) {
            $newName = $request->input('name');
            $actualUser['name'] = $newName;
            $updateUser->name = $newName;
        }
        if ($request->filled('adress')) {
            $newAdress = $request->input('adress');
            $actualUser['adress'] = $newAdress;
            $updateUser->adress = $newAdress;
        }
        if ($request->filled('phone')) {
            $newPhone = $request->input('phone');
            $actualUser['phone'] = $newPhone;
            $updateUser->phone = $newPhone;
        }
        $actualPassword = $request->input('actualPassword');
        if ($request->filled('actualPassword') && $request->filled('password')
            && $request->filled('password_confirmation')) {
                //var_dump($this->user->query()->select('password')->where('email', 'LIKE', '%' . $email . '%')->get());
                //var_dump($this->user->samePasswords($email));
            if ($actualPassword == $actualUser['password']) {
                $newPassword = $request->input('password');

                $actualUser['password'] = $newPassword;
                $updateUser->password = $newPassword;
            }
        }
        if ($request->file('img')) {
            $this->uploadService = $UploadProfilePicService;
            $this->uploadService->uploadFile($request->file('img'));

            $newImg = $request->img->getClientOriginalName();
            $actualUser['img'] = $newImg;
            $updateUser->img = $newImg;
        }

        $request->session()->put('Usuario', $actualUser);
        $updateUser->save();
        return view('profile', with(['user'=>$this->user->validate($request->input('name'), $request->input('adress'),
        $request->input('phone'), $request->input('password'))->get()]));
    }
}
