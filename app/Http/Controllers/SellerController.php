<?php

namespace App\Http\Controllers;
use App\Models\Item;
use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Services\UploadFileService;

use App\Exceptions\UploadFileException;

class SellerController extends Controller
{
    private $uploadService;

    public function show(Request $request) {
        return view('seller');
    }

    public function addProduct(ItemRequest $request, UploadFileService $UploadFileService) {
        $this->uploadService = $UploadFileService;
        $this->uploadService->uploadFile($request->file('img'));
        $item = new Item;
        $item->name = $request->input('name');
        $item->price = $request->input('price');
        $available = $request->input('stock');
        $item->category = $request->input('category');
        $item->size = $request->input('size');
        $item->color = $request->input('color');
        $item->img  = $request->img->getClientOriginalName();

        if ($available == 'Disponible') {
            $item->available = 1;
        } else {
            $item->available = 0;
        }
        $item->save();
        
        $accessories = Item::all();
        return redirect('category')->with(["accessories"=>$accessories]);
    }
}
