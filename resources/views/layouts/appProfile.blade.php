<!DOCTYPE html>

<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{asset('img/favicon.ico')}}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Perfil</title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
  <!-- CSS Files -->
  <link href="{{asset('css/light-bootstrap-dashboard.css?v=2.0.0 ')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{asset('css/demo.css" rel="stylesheet')}}" />


  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <meta content="Metronic Shop UI description" name="description">
  <meta content="Metronic Shop UI keywords" name="keywords">
  <meta content="keenthemes" name="author">

  <meta property="og:site_name" content="-CUSTOMER VALUE-">
  <meta property="og:title" content="-CUSTOMER VALUE-">
  <meta property="og:description" content="-CUSTOMER VALUE-">
  <meta property="og:type" content="website">
  <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
  <meta property="og:url" content="-CUSTOMER VALUE-">

  <link rel="shortcut icon" href="favicon.ico">

  <!-- Fonts START -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css"><!--- fonts for slider on the index page -->
  <!-- Fonts END -->

  <!-- Global styles START -->
  <link href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Global styles END -->

  <!-- Page level plugin styles START -->
  <link href="{{asset('pages/css/animate.css" rel="stylesheet')}}">
  <link href="{{asset('plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet">
  <link href="{{asset('plugins/owl.carousel/assets/owl.carousel.css')}}" rel="stylesheet">
  <!-- Page level plugin styles END -->

  <!-- Theme styles START -->
  <link href="{{asset('pages/css/components.css')}}" rel="stylesheet">
  <link href="{{asset('pages/css/slider.css')}}" rel="stylesheet">
  <link href="{{asset('pages/css/style-shop.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('corporate/css/style.css')}}" rel="stylesheet">
  <link href="{{asset('corporate/css/style-responsive.css')}}" rel="stylesheet">
  <link href="{{asset('corporate/css/themes/red.css')}}" rel="stylesheet" id="style-color">
  <link href="{{asset('corporate/css/custom.css')}}" rel="stylesheet">
  <!-- Theme styles END -->
</head>

<body class="ecommerce">
  <!-- BEGIN HEADER -->
  <div class="header">
    <div class="container">
      <!-- BEGIN TOP SEARCH -->
      <div class="search-box align-self-xl-center">
        <a class="site-logo" href="category">
          <img src="{{asset('corporate/img/logos/logo.png')}}" alt="AFK" style="width: 100%">
        </a>
        <form class="form-search content-search navbar-form" action="/categorySearch" method="post">
            @csrf
          <div class="input-group">
            <span class="input-group-btn">
              <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bars"></i>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="/category/anells">Anells</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/barrets">Barrets</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/bolsos">Bolsos</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/bufandes">Bufandes</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/collars">Collars</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/guants">Guants</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/mascaretes">Mascaretes</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/pendentss">Pendents</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/polseres">Polseres</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/rellotges">Rellotges</a></li>
                  <li class="divider"></li>
                  <li><a href="/category/ulleres">Ulleres</a></li>
                </ul>
              </div>
            </span>
            <input placeholder="Buscar" class="form-control form-text" type="text" size="75" maxlength="128" name="search">
            <span class="input-group-btn">
                <button class="btn btn-primary" type="submit"><span class="icon glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </span>
          </div>
          @if (app('request')->session()->get('Usuario'))
            <a href="/profile" class="btn btn-info btn-lg" style="color:#000; background-color:#fff; border-color:#fff;">
          @else
            <a href="/login" class="btn btn-info btn-lg" style="color:#000; background-color:#fff; border-color:#fff;">
          @endif
            <span class="glyphicon glyphicon-user"></span>
            {{ app('request')->session()->get('Usuario')['name'] ?? 'login'}}
          </a>
        </form>
        <!-- BEGIN CART -->

        <div class="top-cart-block">
            <div class="top-cart-content-wrapper">
              <div class="top-cart-content">
                <ul class="scroller" style="height: 250px;">
                @php
                  $totalPrice = 0;
                  $carrito = app('request')->session()->get('Usuario')['carrito'] ?? [];
                  $id = 0;
                @endphp
                @forelse ($carrito as $producto)
                  <li>
                    <a href="shop-item.html"><img src="{{asset('pages/img/products/').'/'. $producto['img'] }}" alt="$producto['name']" width="37" height="34"></a>
                    <span class="cart-content-count">x 1</span>
                    <strong><a href="shop-item.html">{{ $producto['name'] }}</a></strong>
                    <em>{{ $producto['price'] }}€</em>
                    <a href="/removeProductFromCart/{{$id}}" class="del-goods">&nbsp;</a>
                  </li>
                  @php
                    $totalPrice += $producto['price'];
                    $id++;
                  @endphp
                @empty
                  <p>No hi ha productes a la cistella</p>
                @endforelse
                </ul>
                <div class="text-right">
                  <a href="/removeAllCart" class="btn btn-default">Buidar cistella</a>
                  <a href="/cart" class="btn btn-default">Veure cistella</a>
                  <a href="confirmacion.html" class="btn btn-primary">Confirmar</a>
                </div>
              </div>
            </div>
            <div class="top-cart-info">
              <a class="top-cart-info-count">{{sizeof($carrito)}} productes</a>
              <a class="top-cart-info-value">{{$totalPrice}}€</a>
            </div>
            <i class="fa fa-shopping-cart"></i>
          </div>
          <!--END CART -->
      </div>
      <!-- END TOP SEARCH -->
    </div>
  </div>
  <div class="wrapper">
    <div class="sidebar" data-color="red">
      <div class="sidebar-wrapper">
        <div class="logo">
          <a class="simple-text">
            <a class="simple-text" href="/profile">
              Perfil
            </a>
          </div>
          <ul class="nav">
            <li>
              <a class="nav-link" href="">
                <i class="nc-icon nc-bag"></i>
                <p>Les meves comandes</p>
              </a>
            </li>
            <li>
              <a class="nav-link" href="">
                <i class="nc-icon nc-credit-card"></i>
                <p>Metodes de pagament</p>
              </a>
            </li>
            <li>
              <a class="nav-link" href="">
                <i class="nc-icon nc-notes"></i>
                <p>Cupons</p>
              </a>
            </li>
            <li>
              <a class="nav-link" href="">
                <i class="nc-icon nc-favourite-28"></i>
                <p>Premium</p>
              </a>
            </li>
            <li>
              <a class="nav-link" href="/seller">
                <i class="nc-icon nc-simple-add"></i>
                <p>Afegir Producte</p>
              </a>
            </li>
            <li>
              <a class="nav-link" href="/setting">
                <i class="nc-icon nc-settings-gear-64"></i>
                <p>Configuració</p>
              </a>
            </li>
            <li class="nav-item active active-pro">
              <a class="nav-link active" href="/logout">
                <i class="nc-icon"></i>
                <p>Tancar Sessió</p>
              </a>
            </li>
          </ul>
        </div>
      </div>
      @yield('content')
    </div>
  <!-- BEGIN PRE-FOOTER -->
  <div class="pre-footer">
    <div class="container">
      <div class="row">
        <!-- BEGIN BOTTOM ABOUT BLOCK -->
        <div class="col-md-4 col-sm-6 pre-footer-col">
          <h2>Sobre nosaltres</h2>
          <p>Descripció</p>
        </div>
        <!-- END BOTTOM ABOUT BLOCK -->
        <!-- BEGIN BOTTOM INFO BLOCK -->
        <div class="col-md-4 col-sm-6 pre-footer-col">
          <h2>Informació</h2>
          <ul class="list-unstyled">
            <li><i class="fa fa-angle-right"></i> <a href="">Contacte</a></li>
          </ul>
        </div>
        <!-- END INFO BLOCK -->

        <!-- BEGIN BOTTOM CONTACTS -->
        <div class="col-md-4 col-sm-6 pre-footer-col">
          <h2>Formes de contacte</h2>
          <address class="margin-bottom-40">
            Carrer del Comte d'Urgell, 187<br>
            Barcelona<br>
            Telefón: 93 430 92 00<br>
            Emails:   <a href="mailto:correoDeContacto1@gmail.com">correoDeContacto1@gmail.com</a><br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="mailto:correoDeContacto2@gmail.com">correoDeContacto2@gmail.com</a><br>
          </address>
        </div>
        <!-- END BOTTOM CONTACTS -->
      </div>
    </div>
  </div>
  <!-- END PRE-FOOTER -->

  <!-- BEGIN FOOTER -->
  <div class="footer">
    <div class="container">
      <div class="row">
        <!-- BEGIN COPYRIGHT -->
        <div class="col-md-6 csm-4 padding-top-10">
          2020 © AFK. ALL Rights Rol-eserved.
        </div>
        <!-- END COPYRIGHT -->
      </div>
    </div>
  </div>
  <!-- END FOOTER -->
  </body>
  <!--   Core JS Files   -->
  <script src="{{asset('js/core/jquery.3.2.1.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('js/core/popper.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('js/core/bootstrap.min.js')}}" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="{{asset('js/plugins/bootstrap-switch.js')}}"></script>
  <!--  Google Maps Plugin    -->
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!--  Chartist Plugin  -->
  <script src="{{asset('js/plugins/chartist.min.js')}}"></script>
  <!-- Control Center for Light Bootstrap Dashboard: scripts for the example pages etc -->
  <script src="{{asset('js/light-bootstrap-dashboard.js?v=2.0.0 ')}}" type="text/javascript"></script>
  <!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
  <script src="{{asset('js/demo.js')}}"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    // Javascript method's body can be found in assets/js/demos.js
    demo.initDashboardPageCharts();

  });
</script>
<script src="{{asset('plugins/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('plugins/jquery-migrate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('corporate/scripts/back-to-top.js')}}" type="text/javascript"></script>
<script src="{{asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
<script src="{{asset('plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script><!-- pop up -->
<script src="{{asset('plugins/owl.carousel/owl.carousel.min.js')}}" type="text/javascript"></script><!-- slider for products -->
<script src='{{asset('plugins/zoom/jquery.zoom.min.js')}}' type="text/javascript"></script><!-- product zoom -->
<script src="{{asset('plugins/bootstrap-touchspin/bootstrap.touchspin.js')}}" type="text/javascript"></script><!-- Quantity -->

<script src="{{asset('corporate/scripts/layout.js')}}" type="text/javascript"></script>
<script src="{{asset('pages/scripts/bs-carousel.js')}}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
  Layout.init();
  Layout.initOWL();
  Layout.initImageZoom();
  Layout.initTouchspin();
  Layout.initTwitter();

  Layout.initFixHeaderWithPreHeader();
  Layout.initNavScrolling();
});
</script>

</html>
