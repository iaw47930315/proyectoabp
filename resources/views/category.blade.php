<!-- Dependiendo de si es Ajax o no, cargamos un layout con head y body o no-->
@extends((Request::wantsJson()) ? 'layouts.ajax' : 'layouts.app')

@section('content')
@if(!Request::wantsJson())
<div class="title-wrapper">
    <div class="container">
      <div class="container-inner">
        <h1><span>MEN</span> CATEGORY</h1>
      </div>
    </div>
</div>
<div class="main">
    <div class="container">
        <div class="row margin-bottom-40">
            <div class="sidebar col-md-3 col-sm-5">
                <div class="sidebar-filter margin-bottom-25">
                <h2>Filtres</h2>
                <div class="checkbox-list">
                    <form id="filterForm" action={{ url('/category/') }} method="post">
                        @csrf
                        <h3>Disponibilitat</h3>
                        <label for="notAvailable">No Disponible</label>
                        <input id="notAvailable" type="checkbox" name="notAvailable" value="1{{ old('notAvailable') }}" class="form-control"><br>

                        <label for="inStock">En Stock</label>
                        <input id="inStock" type="checkbox" name="inStock" value="1{{ old('inStock') }}" class="form-control">

                        <h3>Preu</h3>
                        <label for="priceMin">Preu Min</label>
                        <input id="priceMin" type="text" name="priceMin" value="{{ old('priceMin') }}" class="form-control">

                        <label for="priceMax">Preu Max</label>
                        <input id="priceMax" type="text" name="priceMax" value="{{ old('priceMax') }}" class="form-control"><br>

                        <h3>Ordenar per: </h3>
                        <select name="order" id="order" class="form-control">
                            <option value="default">Per defecte</option>
                            <option value="nameAsc">Nom: (A - Z)</option>
                            <option value="nameDesc">Nom: (Z - A)</option>
                            <option value="priceAsc">Preu: Mes barats primer</option>
                            <option value="priceAsc">Preu: Mes cars primer</option>
                        </select><br>

                        <h3>Mostrar: </h3>
                        <select name="show" id="show" class="form-control">
                            <option value="all">Tots</option>
                            <option value="6">6</option>
                            <option value="12">12</option>
                            <option value="18">18</option>
                            <option value="24">24</option>
                        </select><br><br>

                        <input type="hidden" id="category" value="">

                        <input type="submit" class="btn btn-primary" value="Buscar">
                    </form>
                </div>
            </div>
        </div>
        <script>
            $('#filterForm').submit(function(e) {
                e.preventDefault()
                //Datos del formulario lo guardamos como json en data
                var data = $("#filterForm").serialize()
                axios.post('category', data)
                    .then(response => {
                        console.log(response.data)
                        //La respuesta obtenida de la petición Ajax la pegamos en nuestra web
                        //substituyendo el content actual (no el menu de filtros)
                        $('#content').replaceWith(response.data)
                    })
            })
        </script>
        <div class="col-md-9 col-sm-7">
            <div class="row list-view-sorting clearfix">
                    <div class="col-md-12 col-sm-12">
                        <div class="col-md-4 col-sm-4 items-info">{{ $accessories->count() }} artículos</div>
                        <div class="pull-right">
                            {{-- <div class="btn-group">
                                <button type="button" class="btn btn-danger">Show:</button>
                                <button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">12</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">24</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">36</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">48</a>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
        @endif

        <div id="content" class="row product-list">
            @forelse ($accessories as $item)
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="product-item">
                    <div class="pi-img-wrapper">
                        <img src="{{asset('pages/img/products/').'/'. $item->img }}" class="img-responsive" alt="$item->name">
                        <div>
                            <a href="{{asset('pages/img/products/').'/'. $item->img }}" class="btn btn-default fancybox-button">Zoom</a>
                            <a href="#product-pop-up" class="btn btn-default fancybox-fast-view">Veure</a>
                        </div>
                    </div>
                    <h3><a href="shop-item.html">{{ $item->name }}</a></h3>
                    <div class="pi-price">{{ $item->price }}€</div>
                    <form method="post" action={{ url('/addToCart') }}>
                        @csrf
                        <input type="hidden" name="id" value="{{$item->id}}">
                        <input type="hidden" name="name" value="{{$item->name}}">
                        <input type="hidden" name="price" value="{{$item->price}}">
                        <input type="hidden" name="color" value="{{$item->color}}">
                        <input type="hidden" name="size" value="{{$item->size}}">
                        <input type="hidden" name="img" value="{{$item->img}}">
                        <input type="submit" class="btn btn-default add2cart" value="Add to cart">
                    </form>
                </div>
            </div>
            @empty
            <div class="col-md-4 col-sm-6 col-xs-12">
                No hi ha productes disponibles
            </div>
            @endforelse
        </div>
        <!-- BEGIN PAGINATOR
        <div class="row">
            <div class="col-md-4 col-sm-4 items-info"></div>
            <div class="col-md-8 col-sm-8">
            <ul class="pagination pull-right">
                <li><a href="javascript:;">&laquo;</a></li>
                <li><span><a href="javascript:;">1</a></span></li>
                <li><a href="javascript:;">2</a></li>
                <li><a href="javascript:;">3</a></li>
                <li><a href="javascript:;">4</a></li>
                <li><a href="javascript:;">5</a></li>
                <li><a href="javascript:;">&raquo;</a></li>
            </ul>
            </div>
        </div>
         END PAGINATOR -->
    </div>
</div>
</div>
@endsection

