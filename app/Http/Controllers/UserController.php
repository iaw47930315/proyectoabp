<?php

namespace App\Http\Controllers;
use App\Models\Item;
use App\Models\User;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public $user;

    public function __construct() {
        $this->user = new User;
    }

    public function showLogin() {
        return view('login');
    }

    public function loginUser(LoginRequest $request) {
        $email = $request->input('email');
        $password = $request->input('password');
        if ($password != $this->user->loginPassword($email)->value('password')) {
            $errorEmailOrPassword = 'The email or password are incorrect';
            return view('login', with(['user'=>$this->user->validate($email, $password)
            ->get(), "errorEmailOrPassword"=>$errorEmailOrPassword]));
        }
        $name = $this->user->loginName($email)->value('name');
        $img = $this->user->img($email)->value('img');
        $arrayUser = array('img' => $img, 'name' => $name, 'password' => $password, 'email' => $email, 'carrito' => array());
        $request->session()->put('Usuario', $arrayUser);
        $accessories = Item::all();
        return redirect('category')->with(['user'=>$this->user->loginPassword($email, $password)->get(), "accessories"=>$accessories]);
    }

    public function showRegister() {
        return view('register');
    }

    public function authenticate(RegisterRequest $request) {
        $newUser = new User;
        $newUser->name = $request->input('name');
        $newUser->email = $request->input('email');
        $newUser->password = $request->input('password');
        $repeatPassword = $request->input('password_confirmation');
        $newUser->save();

        $img = $this->user->img($newUser->email)->value('img');
        $arrayUser = array('img' => $img, 'name' => $newUser->name, 'password' => $newUser->password, 'email' => $newUser->email, 'carrito' => array());
        $request->session()->put('Usuario', $arrayUser);
        $accessories = Item::all();
        return redirect('category')->with(['user'=>$this->user->validate($newUser->name,
        $newUser->email, $newUser->password, $repeatPassword)->get(), "accessories"=>$accessories]);
    }
}
