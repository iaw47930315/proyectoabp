<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert([
            'name' => 'Polsera',
            'price' => 17.12,
            'category' => 'Polseres',
            'color' => 'vermell',
            'size' => 'L',
            'available' => true,
            'img' => 'bracelet.jpg',
        ]);

        DB::table('items')->insert([
            'name' => 'Barret de palla',
            'price' => 15.49,
            'category' => 'Barrets',
            'color' => 'negre',
            'size' => 'XXL',
            'available' => true,
            'img' => 'hat.jpg',
        ]);

        DB::table('items')->insert([
            'name' => 'Collar estrella',
            'price' => 33.99,
            'category' => 'Collars',
            'color' => 'plata',
            'size' => 'M',
            'available' => false,
            'img' => 'necklace.jpg',
        ]);

        DB::table('items')->insert([
            'name' => 'Collar cor',
            'price' => 39.99,
            'category' => 'Collars',
            'color' => 'or',
            'size' => 'M',
            'available' => true,
            'img' => 'necklace2.png',
        ]);

        DB::table('items')->insert([
            'name' => 'Boina',
            'price' => 15.99,
            'category' => 'Barrets',
            'color' => 'negre',
            'size' => 'L',
            'available' => false,
            'img' => 'boina.jpg',
        ]);

        DB::table('items')->insert([
            'name' => 'Polsera 8',
            'price' => 120.00,
            'category' => 'Polseres',
            'color' => 'plata',
            'size' => 'S',
            'available' => true,
            'img' => 'pulsera.jpg',
        ]);

        DB::table('items')->insert([
            'name' => 'Collar lluna',
            'price' => 16.50,
            'category' => 'Collars',
            'color' => 'plata',
            'size' => 'S',
            'available' => true,
            'img' => 'collar.jpg',
        ]);

        DB::table('items')->insert([
            'name' => 'Ulleres grogues',
            'price' => 22.99,
            'category' => 'Ulleres',
            'color' => 'groc',
            'size' => 'M',
            'available' => false,
            'img' => 'gafas.jpg',
        ]);

        DB::table('items')->insert([
            'name' => 'Ulleres blaves',
            'price' => 28.01,
            'category' => 'Ulleres',
            'color' => 'blau',
            'size' => 'S',
            'available' => true,
            'img' => 'gafas2.png',
        ]);

        DB::table('items')->insert([
            'name' => 'Ulleres',
            'price' => 40.00,
            'category' => 'Ulleres',
            'color' => 'plata',
            'size' => 'S',
            'available' => true,
            'img' => 'gafas3.jpg',
        ]);

        DB::table('items')->insert([
            'name' => 'Gorra adidas',
            'price' => 14.99,
            'category' => 'Barrets',
            'color' => 'negre',
            'size' => 'L',
            'available' => false,
            'img' => 'gorra.jpg',
        ]);

        DB::table('items')->insert([
            'name' => 'Gorra helicopter',
            'price' => 20.00,
            'category' => 'Barrets',
            'color' => 'blau',
            'size' => 'M',
            'available' => true,
            'img' => 'gorra2.jpg',
        ]);

        DB::table('items')->insert([
            'name' => 'Rellotge',
            'price' => 57.00,
            'category' => 'Rellotge',
            'color' => 'negre',
            'size' => 'M',
            'available' => true,
            'img' => 'reloj.jpg',
        ]);
    }
}
