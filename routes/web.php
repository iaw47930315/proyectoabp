<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SellerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Category
Route::get('/category/{category}', [CategoryController::class, "category"]);
Route::get('/category', [CategoryController::class, "filter"]);
Route::post('/categorySearch', [CategoryController::class, "search"]);
Route::post('/category', [CategoryController::class, "filter"]);
Route::post('/category/{category}', [CategoryController::class, "filter"]);
Route::post('/addToCart', [CategoryController::class, "addToCart"]);
Route::get('/cart', [CategoryController::class, "viewCart"]);
Route::get('/removeAllCart', [CategoryController::class, "removeAllCart"]);
Route::get('/removeProductFromCart/{id}', [CategoryController::class, "removeProductFromCart"]);

//Product
Route::get('/product', [ProductController::class, "show"]);

//User
Route::get('/login', [UserController::class, "showLogin"]);
Route::post('/login', [UserController::class, "loginUser"]);

Route::get('/register', [UserController::class, "showRegister"]);
Route::post('/register', [UserController::class, "authenticate"]);

// Profile
Route::get('/profile', [ProfileController::class, "showProfile"]);
Route::get('/logout', [ProfileController::class, "closeSession"]);

Route::get('/setting', [ProfileController::class, "setting"]);
Route::post('/updateSetting', [ProfileController::class, "updateSetting"]);


//Seller
Route::get('/seller', [SellerController::class, "show"]);
Route::post('/addProduct', [SellerController::class, "addProduct"]);
