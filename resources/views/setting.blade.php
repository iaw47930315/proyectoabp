@extends('layouts.appProfile')

@section('content')
<div class="main-panel">
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg " color-on-scroll="500">
    <div class="container-fluid">
      <a class="navbar-brand" href=""> CONFIGURACIÓ </a>
      <button href="" class="navbar-toggler navbar-toggler-right" type="button" style="position:absolute;top:3px" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <br>
        <span class="navbar-toggler-bar burger-lines"></span>
        <span class="navbar-toggler-bar burger-lines"></span>
        <span class="navbar-toggler-bar burger-lines"></span>
      </button>
    </div>
  </nav>
  <!-- End Navbar -->
  <div class="content">
    <div class="main">
      <div class="container">
          @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40">
          <!-- BEGIN CONTENT -->
          <div class="col-md-6 col-sm-6">
            <div class="goods-page">
              <div class="goods-data clearfix">
                <div class="table-wrapper-responsive">
                  <div class="form-input">
                  @php
                    $profile = app('request')->session()->get('Usuario');
                  @endphp
                  <form class="form-default" enctype="multipart/form-data" action="/updateSetting" method="POST">
                    @csrf
                    <label for="name">Nom</label><br><br>
                    <ul>
                      <li>Nou Nom: <input type="text" name="name"></li><br>
                      Nom actual: {{$profile['name'] ?? ''}}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            </div>

            <div class="col-md-6 col-sm-6">
            <div class="goods-page">
              <div class="goods-data clearfix">
                <div class="table-wrapper-responsive">
                  <div class="form-input">
                    <label for="name">Direcció</label><br><br>
                    <ul>
                      <li>Canviar Direcció: <input type="text" name="adress"></li><br>
                      Direcció actual: {{$profile['adress'] ?? 'No hi ha cap direcció guardada'}}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            </div>

            <div class="col-md-6 col-sm-6">
            <div class="goods-page">
              <div class="goods-data clearfix">
                <div class="table-wrapper-responsive">
                  <div class="form-input">
                    <label for="name">Telefón</label><br><br>
                    <ul>
                      <li>Canviar Telefón: <input type="text" name="phone"></li><br>
                      Telefón actual: {{$profile['phone'] ?? 'No hi ha cap telefón guardat'}}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            </div>

            <div class="col-md-6 col-sm-6">
            <div class="goods-page">
              <div class="goods-data clearfix">
                <div class="table-wrapper-responsive">
                  <div class="form-input">
                    <label for="name">Contrasenya</label><br><br>
                    <ul>
                      <li>Contrasenya Actual: <input type="password" name="actualPassword"></li><br>
                      <li>Nova Contrasenya: <input type="password" name="password"></li><br>
                      <li>Repetir Contrasenya: <input type="password" name="password_confirmation"></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            </div>

            <div class="col-md-12 col-sm-12">
            <div class="goods-page">
              <div class="goods-data clearfix">
                <div class="table-wrapper-responsive">
                  <div class="form-input">
                    <label for="name">Imatge de Perfil</label><br><br>
                    <ul>
                      <li>Canviar Imatge de Perfil: <br><br><input type="file" class="btn btn-primary" name="img" id="img" style="float: left"></li><br>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <button class="btn btn-primary" type="submit">Guardar canvis <i class="fa fa-check"></i></button>
            </div>
            </form>
          </div>
          <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->


      </div>
    </div>
  </div>
</div>
@endsection
