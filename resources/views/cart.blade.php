@extends('layouts.app')
@section('content')
<div class="main">
  <div class="container">
    <!-- BEGIN SIDEBAR & CONTENT -->
    <div class="row margin-bottom-40">
      <!-- BEGIN CONTENT -->
      <div class="col-md-12 col-sm-12">
        <h1>Carrito</h1>
        <div class="goods-page">
          <div class="goods-data clearfix">
            <div class="table-wrapper-responsive">
              <table summary="Shopping cart">
                <tr>
                  <th class="goods-page-image"></th>
                  <th class="goods-page-description">Descripció</th>
                  <th class="goods-page-ref-no">Referencia</th>
                  <th class="goods-page-quantity">Quantitat</th>
                  <th class="goods-page-price">Preu/U</th>
                  <th class="goods-page-total" colspan="2">Total</th>
                </tr>

                @php
                  $totalPrice = 0;
                  $carrito = app('request')->session()->get('Usuario')['carrito'] ?? [];
                  $id = 0;
                @endphp
                @forelse ($carrito as $producto)
                  <tr>
                    <td class="goods-page-image">
                      <a href="javascript:;"><img src="{{asset('pages/img/products/').'/'. $producto['img'] }}" alt="$producto['name']"></a>
                    </td>
                    <td class="goods-page-description">
                      <h3><a href="javascript:;">{{$producto['name']}}</a></h3>
                      <p>Color: {{$producto['color']}}<br>Talla: {{$producto['size']}}</p>
                    </td>
                    <td class="goods-page-ref-no">
                      {{$producto['id']}}
                    </td>
                    <td class="goods-page-quantity">
                      <div class="product-quantity">
                        <input id="product-quantity" type="text" value="1" readonly class="form-control input-sm">
                      </div>
                    </td>
                    <td class="goods-page-price">
                      <strong>{{$producto['price']}}<span>€</span></strong>
                    </td>
                    <td class="goods-page-total">
                      <strong>{{$producto['price']}}<span>€</span></strong>
                    </td>
                    <td class="del-goods-col">
                      <a class="del-goods" href="/removeProductFromCart/{{$id}}">&nbsp;</a>
                    </td>
                  </tr>
                  @php
                    $totalPrice += $producto['price'];
                    $id++;
                  @endphp
                @empty
                  <p>No hi ha productes a la cistella</p>
                @endforelse


              </table>
            </div>

            <div class="shopping-total">
              <ul>
                <li class="shopping-total-price">
                  <em>Total</em>
                  <strong class="price">{{$totalPrice}}<span>€</span></strong>
                </li>
              </ul>
            </div>
          </div>
          <a href="/category"><button class="btn btn-default" type="submit">Continuar comprant<i class="fa fa-shopping-cart"></i></button></a>
          <a href="confirmacion.html"><button class="btn btn-primary" type="submit">Confirmar<i class="fa fa-check"></i></button></a>
        </div>
      </div>
      <!-- END CONTENT -->
    </div>
    <!-- END SIDEBAR & CONTENT -->
  </div>
</div>
@endsection
