@extends('layouts.appProfile')

@section('content')
<div class="main-panel">
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg " color-on-scroll="500">
    <div class="container-fluid">
      <a class="navbar-brand" href=""> PERFIL </a>
      <!-- <button href="" class="navbar-toggler navbar-toggler-right" type="button" style="position:absolute;top:3px" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <br>
        <span class="navbar-toggler-bar burger-lines"></span>
        <span class="navbar-toggler-bar burger-lines"></span>
        <span class="navbar-toggler-bar burger-lines"></span>
      </button> -->
    </div>
  </nav>
  <!-- End Navbar -->
  @php
    $profile = app('request')->session()->get('Usuario');
  @endphp
  <div class="content">
    <div class="goods-page">
      <div class="goods-data clearfix" style="display: flex; align-items: center">
        <div class="table-wrapper-responsive" style="width: 50%">
          <img src="{{asset('profilePics/').'/'. $profile['img']}}" alt="AFK" style="max-width: 400px">
        </div>
        <div style="width: 40%; margin-left: 5%; font-size: 18px">
          <h1><b>Les Meves Dades</b></h1><br>
          <label for="name">Nom:</label>&nbsp;{{$profile['name'] ?? ''}}<br><br>
          <label for="email">Email:</label>&nbsp;{{$profile['email'] ?? ''}}<br><br>
          <label for="address">Direcció:</label>&nbsp;{{$profile['adress'] ?? ''}}<br><br>
          <label for="phone">Telefon:</label>&nbsp;{{$profile['phone'] ?? ''}}
      </div>
      </div>
    </div>
  </div>
</div>
@endsection
