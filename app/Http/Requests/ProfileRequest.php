<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'max:20|nullable',
            'adress' => 'min:10|nullable',
            'phone' => 'unique:users|nullable',
            'password' => 'confirmed|min:6|nullable'
        ];
    }

    public function messages()
    {
        return [
            'name.max' => 'El camp nom ha de tindre com a màxim de 20 caràcters.',
            'adress.min' => 'El camp direcció ha de tindre com a mínim 10 caràcters.',
            'phone.unique' => 'El teléfon introduit ja s\'esta utilitzant.',
            'password.confirmed' => 'El camp nova contrasenya no coincideix amb el camp repetir contrasenya.',
            'password.min' => 'El camp contrasenya ha de tindre com a mínim 6 caràcters.',
        ];
    }
}
