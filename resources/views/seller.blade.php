@extends('layouts.appProfile')
@section('content')
<div class="main">
    <div class="container">
        <!-- BEGIN CONTENT -->
        <div class="row margin-bottom-40">
            <div class="col-md-11 col-sm-7">
                <div class="product-page">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <form enctype="multipart/form-data" action="/addProduct" method="POST">
                                @csrf
                                <input type="file" class="btn btn-primary" name="img" id="img">
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label class="control-label">Nom:</label>
                            <input type="text" placeholder="Nombre del producto" name="name" class="form-control">
                            <label class="control-label">Preu:</label>
                            <input type="number" placeholder="Precio" name="price" class="form-control">
                            <label class="control-label">Disponibilitat:</label>
                            <select class="form-control" name="stock">
                                <option selected>Disponible</option>
                                <option>No disponible</option>
                            </select>
                            <label class="control-label">Categoria:</label>
                            <select class="form-control" name="category">
                                <option selected>Anells</option>
                                <option>Barrets</option>
                                <option>Bolsos</option>
                                <option>Bufandes</option>
                                <option>Collars</option>
                                <option>Guants</option>
                                <option>Mascaretes</option>
                                <option>Pendents</option>
                                <option>Polseres</option>
                                <option>Rellotges</option>
                                <option>Ulleres</option>
                            </select>

                            <label class="control-label">Talla disponible:</label><br>
                            <input type="text" placeholder="Talla disponible" name="size" class="form-control">

                            <label class="control-label">Color disponible:</label><br>
                            <input type="text" placeholder="Color disponible" name="color" class="form-control">
                            <br><br>

                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Pujar producte</button>
                            </span>
                        </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
