<?php

namespace App\Http\Controllers;
use App\Models\Item;

use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;

class CategoryController extends Controller
{
    public $item;

    public function __construct() {
        $this->item = new Item;
    }

    public function search(Request $request) {
        $search = $request->input('search');
        return view('category', with(['accessories'=>$this->item->search($search)->get()]));
    }

    public function category($category) {
        //$search = $request->input('search');
        $items = $this->item->category($category)->get();
        return view('category')->with('accessories', $items);
    }

    public function filter(ItemRequest $request)
    {
        $request->flash();

        $items = $this->item->query();

        if ($request->filled('priceMin')) {
            $items->priceMin($request->input('priceMin'));
        }

        if ($request->filled('priceMax')) {
            $items->priceMax($request->input('priceMax'));
        }

        if ($request->isNotFilled('notAvailable') || $request->isNotFilled('inStock')) {
            if ($request->filled('notAvailable')) {
                $search = false;
                $items->available($search);
            } else if ($request->filled('inStock')) {
                $search = true;
                $items->available($search);
            }
        }

        if ($request->filled('order')) {
            $orderInput = $request->input('order');
            if ($orderInput != 'default') {
                if ($orderInput == 'nameAsc') {
                    $order = 'ASC';
                    $search = 'name';
                } else if ($orderInput == 'nameDesc') {
                    $order = 'DESC';
                    $search = 'name';
                } else if ($orderInput == 'priceAsc') {
                    $order = 'ASC';
                    $search = 'price';
                } else {
                    $order = 'DESC';
                    $search = 'price';
                }
                $items->order($search, $order);
            }
        }

        if ($request->filled('show')) {
            $showInput = $request->input('show');
            if ($showInput != 'all') {
                $items->show($showInput);
            }
        }
        return view('category')->with('accessories', $items->get());
    }

    public function addToCart(ItemRequest $request)
    {
        if (null === app('request')->session()->get('Usuario')) {
            return view('login');
        }
        $user = app('request')->session()->get('Usuario');
        $producto = array('id' => $request->input('id'), 'name' => $request->input('name'),
                       'price' => $request->input('price'), 'color' => $request->input('color'),
                       'size' => $request->input('size'), 'img' => $request->input('img'));

        array_push($user['carrito'], $producto);

        $request->session()->put('Usuario', $user);

        return redirect('category');
    }

    public function removeProductFromCart(ItemRequest $request, $id)
    {
        $user = app('request')->session()->get('Usuario');
        
        array_splice($user['carrito'], $id, 1);

        $request->session()->put('Usuario', $user);

        return redirect(url()->previous());
    }

    public function removeAllCart(ItemRequest $request)
    {
        if (null === app('request')->session()->get('Usuario')) {
            return view('login');
        }
        $user = app('request')->session()->get('Usuario');
        
        $user['carrito'] = array();

        $request->session()->put('Usuario', $user);

        return redirect('category');
    }

    public function viewCart(ItemRequest $request)
    {
        if (null === app('request')->session()->get('Usuario')) {
            return view('login');
        }

        return view('cart');
    }
}