<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:20',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed|min:6',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El camp nom és obligatori.',
            'name.max' => 'El camp nom ha de tindre com a màxim 20 caràcters.',
            'email.required' => 'El camp email és obligatori.',
            'email.unique' => 'El email introduit ja s\'esta utilitzant.',
            'password.required' => 'El camp contrasenya és obligatori.',
            'password.confirmed' => 'El camp contrasenya no coincideix amb el camp repetir contrasenya.',
            'password.min' => 'El camp contrasenya ha de tindre com a mínim 6 caràcters.',
        ];
    }
}
