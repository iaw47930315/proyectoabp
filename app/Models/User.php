<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeLoginPassword($query, $user) {
        return $query -> select('password')->where('email', 'LIKE', '%' . $user . '%');
        //-> where('email', 'LIKE', '%' . $user . '%')
            //->orWhere('password', 'LIKE', '%' . $user . '%');
    }

    public function scopeLoginName($query, $user) {
        return $query -> select('name')->where('email', 'LIKE', '%' . $user . '%');
    }

    public function scopeValidate($query, $user) {
        return $query -> where('name', 'LIKE', '%' . $user . '%')
            ->orWhere('email', 'LIKE', '%' . $user . '%')
            ->orWhere('password', 'LIKE', '%' . $user . '%');
    }

    public function scopeSamePasswords($query, $user) {
        return $query -> select('password')->where('email', 'LIKE', '%' . $user . '%');
    }

    public function scopeImg($query, $user) {
        return $query -> select('img')->where('email', 'LIKE', '%' . $user . '%');
    }

    public function scopeId($query, $user) {
        return $query -> select('id')->where('email', 'LIKE', '%' . $user . '%');
    }
}
