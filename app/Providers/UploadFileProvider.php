<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\UploadFileService;
use App\Services\UploadProfilePicService;

class UploadFileProvider extends ServiceProvider
{
    protected $defer = false;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    public function register()
    {
        $this->app->bind('App\Services\UploadFileService', function ($app) {
            return new UploadFileService();
        });
        $this->app->bind('App\Services\UploadProfilePicService', function ($app) {
            return new UploadProfilePicService();
        });
    }
}
